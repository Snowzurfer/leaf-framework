package leaffmk.test_states
{
	import flash.net.drm.DRMPlaybackTimeWindow;
	import leaffmk.BaseState;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import leaffmk.entities.Player;
	import leaffmk.input.Keyboard;
	import flash.events.Event;
	import leaffmk.graphics.Background;
	import leaffmk.maths.Vector2
	import leaffmk.Constants;
	
	/**
	 * In-game state
	 * @author Alberto Taiuti
	 */
	public class Play extends BaseState
	{
		// The mutli-scrolling background object
		private var _bg0:Background;
		
		// The player
		private var _player:Player;
		
		/**
		 * The mighty constructor
		 */
		public function Play() {
			super();
			
			_stateID = Constants.STATE_INGAME;
			
			var vect1:Vector2 = new Vector2(10, 0);
			var vect2:Vector2 = new Vector2(0, 0);
			_bg0 = new Background();
			_player = new Player();
			
			_running = true;
		}
		
		/**
		 * Init graphics for all the objects
		 */
		override public function initGraphics():void {
			_bg0.initGraphics();
			_player.initGraphics();
		}
		
		/**
		 * Update the game state
		 * @param	dt	DeltaTime
		 */
		override public function update(dt:Number):void {
			// Update only if not paused
			if (_running) {
				_bg0.update(dt, 300);
				_player.update(dt);
			}
		}
		
		/**
		 * Detect user input
		 */
		override public function handleInput():void {
			if (Keyboard.isJustPressed(Keyboard.SPACEBAR)) {
				this.dispatchEvent(new Event("changeState"));
				
				_running = false;
			}
			_player.handleInput();
		}
		
		/**
		 * Render objects
		 */
		override public function render():void {
			// Render background
			_bg0.render();
			
			// Render everything else
			_player.render();
			
			// Render UI
		}
	}
}