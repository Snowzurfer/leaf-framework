package leaffmk.test_states
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	import leaffmk.custom_events.LmStateEvent;
	import flash.text.TextField;
	import leaffmk.BaseState;
	import flash.display.Bitmap;
	import flash.display.Stage;
	import leaffmk.graphics.Assets;
	import leaffmk.input.Keyboard;
	import leaffmk.UI.LmTextButton;
	import leaffmk.Constants;
	import leaffmk.custom_events.LmBtnEvent;
	
	/**
	 * Sample main menu for a game
	 * @author Alberto Taiuti
	 */
	public class MainMenu extends BaseState
	{	
		// Has a background
		private var _backgroundBM:Bitmap;
		
		// Has some text
		//private var _textField:TextField;
		
		
		/**
		 * The mighty constructor
		 */
		public function MainMenu() {
			super();
			
			_stateID = Constants.STATE_MENU;
			
			// Add the background
			_backgroundBM = new Assets.bgMainMenu();
			
			// Add start button
			var playBtn:LmTextButton = new LmTextButton("playBtn", (Main.renderManager.STAGE_WIDTH / 2) - 95, 200, 210, 69, "Play", new TextFormat("Buxton Sketch", 36, 0xFFFFFF, true), Assets.btnMainMenuNohover, Assets.btnMainMenuHover);
			playBtn.addEventListener(LmBtnEvent.BTN_CLICK, onMouseClick);
			
			_buttons.push(playBtn);
			
			
			
			// Add options button
			
			// Add about button
			
			_running = true;
		}
		
		
		override public function handleInput():void {
			if (Keyboard.isJustPressed(Keyboard.SPACEBAR))
			{
				this.dispatchEvent(new LmStateEvent(LmStateEvent.CHANGE_STATE, Constants.STATE_INGAME));
				
				_running = false;
			}
		}
		
		override public function update(dt:Number):void {
			// Update only if not stopped
			if (_running)
			{
				super.update(dt);
			}
		}
		
		override public function render():void {
			// Render background
			Main.renderManager.drawObject(_backgroundBM, 0, 0);
			
			// Render everything else
			
			// Render UI
			super.render();
		}
		
		private function onMouseClick(e:LmBtnEvent):void {
			// TODO fix name problem
			
			if (e.dispatcherName == "playBtn") {
				this.dispatchEvent(new LmStateEvent(LmStateEvent.CHANGE_STATE, Constants.STATE_INGAME));
				
				_running = false;
			}
		}
	}
}