package leaffmk 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import leaffmk.maths.Vector2;
	
	/**
	 * The very basic object of the leaf framework. Can either be moving or not.
	 * Has properties that each entity/object shares. (So it can either be a rock,
	 * a character, etc).
	 * @author Alberto Taiuti
	 */
	public class BasicObject extends EventDispatcher
	{
		// Movable or not
		protected var _isMoveable:Boolean;
		
		// Velocity, position, ecc
		protected var _acc:Vector2;
		protected var _vel:Vector2;
		protected var _pos:Vector2;
		protected var _size:Vector2;
		
		// Settings
		protected var _hasWorldCollisions:Boolean;
		protected var _applyGravity:Boolean;
		protected var _applyFriction:Boolean;
		
		/**
		 * The mighty constructor
		 */
		public function BasicObject() {
			//Initialize to 0 the vel etc by default
			_vel = new Vector2(0, 0);
			_pos = new Vector2(0, 0);
			_size = new Vector2(1, 1);
			_acc = new Vector2(0, 0);
			
			_hasWorldCollisions = false;
			_applyFriction = false;
			_applyGravity = false;
		}
		
		public function initGraphics():void {
			
		}
		
		
		public function handleInput():void {
			
		}
		
		public function update(dt:Number):void {
			// If the object is moveable
			/*if (_isMoveable) {
				// Cap velocity
				if(_vel.xComp < -
				_pos.AddTo(_vel); // Update postion based on velocity
			}*/
		}
		
		public function render():void {
			// Render background
			
			// Render everything else
			
			// Render UI
		}
		
		
		// Getters and setters
		public function get size():Vector2 {
			return _size;
		}
		
		public function set size(value:Vector2):void {
			_size = value;
		}
		
		public function get pos():Vector2 {
			return _pos;
		}
		
		public function set pos(value:Vector2):void {
			_pos = value;
		}
		
		public function get isMoveable():Boolean {
			return _isMoveable;
		}
		
		public function set isMoveable(value:Boolean):void {
			_isMoveable = value;
		}
		
		public function get vel():Vector2 {
			return _vel;
		}
		
		public function set vel(value:Vector2):void {
			_vel = value;
		}
		
		
		
	}

}