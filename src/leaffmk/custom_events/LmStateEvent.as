package leaffmk.custom_events
{
	import flash.events.Event;
	
	/**
	 * Custom event for state changes
	 * @author Alberto Taiuti
	 */
	public class LmStateEvent extends Event 
	{
		// Event types
		public static const CHANGE_STATE:String = "changeState";
		
		// Target state to be set when this event is dispatched
		private var _nextState:int;
		
		public function LmStateEvent(type:String, nextState:int, bubbles:Boolean=false, cancelable:Boolean=true) {
			super(type, bubbles, cancelable);
			
			_nextState = nextState;
		}
		
		
		public function get nextState():int {
			return _nextState;
		}
		
		public function set nextState(value:int):void {
			_nextState = value;
		}
		
	}

}