package leaffmk.graphics
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.filters.*;
	import flash.geom.Rectangle;
	
	/**
	 * Renders the graphics to the screen
	 * @author Alberto Taiuti
	 */
	public class RenderManager
	{
		// Bitmap to be displayed (added to the displayObject stack)
		private var _bitmap:Bitmap;
		
		// BitmapData used as a buffer; graphics are drawn on
		// it and only "bitmap" is added to the stage
		private var _buffer:BitmapData;
		
		// Matrix manipulator
		private var _matrix:Matrix;
		
		// Hold the screen's widht and height for this renderer
		public var STAGE_HEIGHT:int;
		public var STAGE_WIDTH:int;
		
		
		public function RenderManager(stageWidth:int, stageHeight:int) {
			
			// Create the buffer
			_buffer = new BitmapData(stageWidth, stageHeight, false, 0xFFFFFF);
			
			// Create the bitmap to be added to the stack (displayObject stack) using the buffer
			_bitmap = new Bitmap(_buffer);
			
			STAGE_HEIGHT = stageHeight;
			STAGE_WIDTH = stageWidth;
			
			trace("RenderManager initialized.");
		}
		
		/**
		 * Draw a sprite onto the buffer.
		 * @param	sprite		Sprite to be drawn
		 * @param	xPos		Xposition of the sprite to be drawn
		 * @param	yPos		Y position of the sprite to be drawn
		 * @param	scaleX		X scaling factor	
		 * @param	scaleY		Y scaling factor
		 * @param	rotn		Angle of rotation
		 */
		public function drawObject(sprite:DisplayObject, xPos:Number, yPos:Number, scaleX:Number = 1, scaleY:Number = 1, rotn:Number = 0):void {
			// Create a new transf. matrix and transform it
			_matrix = new Matrix();
			_matrix.scale(scaleX, scaleY);
			_matrix.translate(xPos, yPos);
			_matrix.rotate(rotn);
			
			// Render sprite on the buffer
			_buffer.draw(sprite, _matrix);
		}
		
		/**
		 * Clear the screen with the desired colour
		 * @param	colour
		 */
		public function clearScreen(colour:String):void {
			// Create a new bitmapdata with the desired colour
			_buffer.fillRect(new Rectangle(0, 0, STAGE_WIDTH, STAGE_HEIGHT), uint(colour));
		}
		
		
		// Setters and getters
		/*public function get buffer():BitmapData
		{
			return _buffer;
		}
		public function set buffer(value:BitmapData):void
		{
			_buffer = value;
		}*/
		
		public function get bitmap():Bitmap {
			return _bitmap;
		}
		public function set bitmap(value:Bitmap):void {
			_bitmap = value;
		}
		
		
	}

}