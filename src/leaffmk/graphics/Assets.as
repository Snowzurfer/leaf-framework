package leaffmk.graphics 
{
	import flash.utils.Dictionary;
	/**
	 * Contains definitions and embeddeds for all the assets.
	 * This way the embedded files will not have loading problems being loaded
	 * everywhere in the code.
	 * @author Alberto Taiuti
	 */
	public class Assets 
	{
		// Background assets
		[Embed(source="../../../img/bg_riverside.png")]
		public static const bgRiverside:Class;
		
		[Embed(source = "../../../img/bg_riverside_close.png")]
		public static const bgRiversideClose:Class;
		
		[Embed(source = "../../../img/bg_sky.png")]
		public static const bgSky:Class;
		
		[Embed(source = "../../../img/bg_riverbank.png")]
		public static const bgRiverBank:Class;
		
		[Embed(source = "../../../img/bg_mainmenu.png")]
		public static const bgMainMenu:Class;
		
		
		// Buttons textures
		[Embed(source="../../../img/menu_btnNoHover.png")]
		public static const btnMainMenuNohover:Class;
		
		[Embed(source = "../../../img/menu_btnHover.png")]
		public static const btnMainMenuHover:Class;
		
		// Player assets
		
		
		//private static var gameTextures:Dictionary = new Dictionary();
		
		/*public static function getTexture(fileName:String) {
			// If the requested texture has not been added to the 
			
			
		}*/
		
	}

}