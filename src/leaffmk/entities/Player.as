package leaffmk.entities 
{
	import flash.display.Shape;
	import leaffmk.BasicObject;
	import leaffmk.maths.Vector2;
	import leaffmk.input.Keyboard;
	import leaffmk.Constants;
	
	/**
	 * Player class
	 * @author Alberto Taiuti
	 */
	public class Player extends BasicObject
	{
		// Player states
		private var _pState:int;
		private const PSTATE_IDLE:int = 0;
		private const PSTATE_ROWING:int = 1;
		private const PSTATE_HITTEN:int = 2;
		
		// Graphics
		private var _testSquare:Shape;
		
		// Define the minimum horizontal speed of the player
		private const MIN_SPEED:Number = 640;
		
		
		
		/**
		 * Mighty constructor
		 */
		public function Player() {
			_isMoveable = true;
			_vel = new Vector2(0, 0);
			_pos = new Vector2(100, (Constants.GAME_AREA.top + 50));
			_size = new Vector2(64, 32);
			
			_isMoveable = true;
			_hasWorldCollisions = true;
			
			_pState = PSTATE_ROWING;
			
			_testSquare = new Shape();
		}
		
		override public function initGraphics():void {
			_testSquare.graphics.lineStyle(1, 0xFF0000, 1);
			_testSquare.graphics.beginFill(0xFFFFFF, 0.9);
			_testSquare.graphics.drawRoundRect(0, 0, 64, 32, 50, 25);
			_testSquare.graphics.endFill();
			
		}
		
		
		override public function handleInput():void {
			if (Keyboard.isDown(Keyboard.UP) && Keyboard.isDown(Keyboard.DOWN)) {
				_vel.yComp = 0; // If both keys are pressed, do nothing
			}
			else if (Keyboard.isDown(Keyboard.UP)) {
				_vel.yComp = -100;
			}
			else if (Keyboard.isDown(Keyboard.DOWN)) {
				_vel.yComp = 100;
			}
			// If none are pressed
			else {
				_vel.yComp = 0; // Stop
			}
		}
		
		override public function update(dt:Number):void {
			// Slowly reduce horizontal speed
			_vel.xComp -= (_vel.xComp - MIN_SPEED) * 0.01;
			
			// Move vertically
			_pos.yComp += _vel.yComp * dt;
			
			// Cap vertical position
			if (_pos.yComp < Constants.GAME_AREA.top + (_size.yComp * 0.3)) {
				_pos.yComp = Constants.GAME_AREA.top + (_size.yComp * 0.3);
			}
			else if (_pos.yComp > Constants.GAME_AREA.bottom - _size.yComp) {
				_pos.yComp = Constants.GAME_AREA.bottom - _size.yComp;
			}
			
			
		}
		
		/**
		 * Render the player
		 */
		override public function render():void {
			Main.renderManager.drawObject(_testSquare, _pos.xComp, _pos.yComp);
		}
		
	}

}